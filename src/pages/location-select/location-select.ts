import {NavParams, Platform} from 'ionic-angular';
import {Component, ElementRef, ViewChild} from '@angular/core';
import {GoogleMaps} from '../../services/googlemaps.service';
import LatLng = google.maps.LatLng;
import Place = google.maps.Place;
import {EventDTO} from "../../services/event-service";

@Component({
  selector: 'page-location-select',
  templateUrl: 'location-select.html'
})
export class LocationSelect {

    @ViewChild('map') mapElement: ElementRef;
    @ViewChild('pleaseConnect') pleaseConnect: ElementRef;

    map: any;
    public latitude: number;
    public longitude: number;
    public locationText: string;
    //autoCompleteService: any;
    //placesService: any;
    query: string = '';
    places: any = [];
    searchDisabled: boolean;
    saveDisabled: boolean;
    location: any;
    public eventDTO: EventDTO;

  constructor(
    //public zone: NgZone,
    public googleMaps: GoogleMaps,
    public platform: Platform,
    //public viewCtrl: ViewController,
    public navParams: NavParams)
  {
    this.eventDTO = this.navParams.get('eventKey');
    this.searchDisabled = true;
    this.saveDisabled = true;
  }

  ionViewDidLoad(): void {
    let eventPlace: Place = this.loadEventPlace();

    this.googleMaps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement, eventPlace).then(() => {

      //this.autoCompleteService = new google.maps.places.AutocompleteService();
      //this.placesService = new google.maps.places.PlacesService(this.googleMaps.map);
      this.searchDisabled = false;

      //let content = "<h4>Aqui Estoy</h4>";
      //this.addMarker();
      //this.addInfoWindow(marker, content);
    });

  }

    /*
    showEventPlace()
    {
      let eventPlace: Place = this.loadEventPlace();
      this.googleMaps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement, eventPlace);
    }
    */

    loadEventPlace(): Place
    {
      let eventPlace: Place;

      if(this.eventDTO)
      {
        this.locationText = this.eventDTO.eventloca;
        this.latitude = this.eventDTO.eventlati;
        this.longitude = this.eventDTO.eventlong;
        //{location: new LatLng(3.429955, -76.541237), placeId: eventDTO.eventloca};
        eventPlace = {location: new LatLng(this.eventDTO.eventlati, this.eventDTO.eventlong), placeId: this.eventDTO.eventloca};
      }
      else
      {
        eventPlace = null;
      }

      return eventPlace;
    }

    /*
    selectPlace(place){

        this.places = [];

        let location = {
            lat: null,
            lng: null,
            name: place.name
        };

        this.placesService.getDetails({placeId: place.place_id}, (details) => {

            this.zone.run(() => {

                location.name = details.name;
                location.lat = details.geometry.location.lat();
                location.lng = details.geometry.location.lng();
                this.saveDisabled = false;

                this.googleMaps.map.setCenter({lat: location.lat, lng: location.lng});

                this.location = location;

            });

        });

    }

    searchPlace(){

        this.saveDisabled = true;

        if(this.query.length > 0 && !this.searchDisabled) {

            let config = {
                types: ['geocode'],
                input: this.query
            };

            this.autoCompleteService.getPlacePredictions(config, (predictions, status) => {

                if(status == google.maps.places.PlacesServiceStatus.OK && predictions){

                    this.places = [];

                    predictions.forEach((prediction) => {
                        this.places.push(prediction);
                    });
                }

            });

        } else {
            this.places = [];
        }

    }


    save(){
        this.viewCtrl.dismiss(this.location);
    }

    close(){
        this.viewCtrl.dismiss();
        this.viewCtrl.dismiss(this.close);
    }
    */

}
