import {Component} from "@angular/core";
import {AlertController, MenuController, NavController, NavParams, ToastController} from "ionic-angular";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import {Page} from "ionic-angular/navigation/nav-util";
import {Settings} from "../../services/settings";
import {LoginService} from "../../services/login-service";
import {FingerprintAIO} from "@ionic-native/fingerprint-aio";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public txtEmail: string = 'cardelgo@hotmail.com';
  public txtPassword: string = '123';

  constructor(
    public nav: NavController,
    public alertController: AlertController,
    public menu: MenuController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public settings: Settings,
    public loginService: LoginService,
    private faio: FingerprintAIO)
  {
    this.menu.swipeEnable(true);

    console.info('Init Login ->');
    console.info(this.settings.allSettings);
    let userId: string = this.settings.allSettings.userId;
    if(userId != null)
    {
      faio.isAvailable()
        .then(() =>
        {
          this.faio.show(
            {
              clientId: 'Fingerprint-Demo',
              clientSecret: 'password', // Only Android
              localizedFallbackTitle: 'Use Pin', // Only iOS
              localizedReason: 'Please authenticate' // Only iOS
            })
            .then((result: any) =>
            {
              console.log('OK with the fingerprint ' + result);
              this.settings.setValue('connected', true);
              this.flowPageParams()
            })
            .catch((error: any) => {
              console.log('Error with the fingerprint', error);
            });
        })
        .catch(reason => {
          console.log('Fingerprint reader not available', reason);
        });
      }

  }

  // go to register page
  register()
  {
    let targetPage : Page = this.navParams.get('targetPage');
    let generalParam : any = this.navParams.get('generalParam');

    this.nav.push(RegisterPage, {targetPage: targetPage, generalParam : generalParam});
  }

  // authenticate and go to home page
  login()
  {
    this.loginService.authenticate(this.txtEmail, this.txtPassword).then(value => {
      if(value['status'] === 1)
      {
        console.info("Authenticated->");
        console.info(value);

        this.settings.allSettings.userId = this.txtEmail;
        this.settings.allSettings.userName = 'Enrique II'; //value['usuanomb']
        this.settings.allSettings.connected = true;
        this.settings.save().then(() =>
          this.flowPageParams());
      }
      else
      {
        let errorToast = this.toastCtrl.create({
          message: 'Invalid credentials',
          duration: 3000,
          position: 'top',
          cssClass: 'dark-trans',
          closeButtonText: 'OK',
          showCloseButton: true
        });
        errorToast.present();
      }
    })
  }

  private flowPageParams()
  {
    let targetPage : Page = this.navParams.get('targetPage');
    let generalParam : any = this.navParams.get('generalParam');
    if(targetPage == undefined)
    {
      this.nav.setRoot(HomePage);
    }
    else
    {
      this.nav.push(targetPage, {generalParam : generalParam});
    }
  }

  forgotPass() {
    let forgot = this.alertController.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked: ' + data['email']);
            let toast = this.toastCtrl.create({
              message: 'Email was sent successfully to ' + data['email'],
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}


