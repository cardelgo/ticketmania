import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {Storage} from '@ionic/storage';

@Component({
  selector: 'page-search-location',
  templateUrl: 'search-location.html'
})

export class SearchLocationPage {
  public fromto: any;
  // places
  public places = {
    nearby: [
      {
        id: 1,
        name: "Current Location"
      },
      {
        id: 2,
        name: "Buenaventura, Valle"
      },
      {
        id: 3,
        name: "Cali, Valle"
      },
      {
        id: 4,
        name: "Bogotá, Cundinamarca"
      },
      {
        id: 5,
        name: "Medellín, Antioquia"
      },
      {
        id: 6,
        name: "Pereira, Risaralda"
      }
    ],
    recent: [
      {
        id: 3,
        name: "Cali, Valle"
      }
    ]
  };

  constructor(private storage: Storage, public nav: NavController, public navParams: NavParams) {
    this.fromto = this.navParams.data;
  }

  // search by item
  searchBy(item) {
    if (this.fromto === 'from') {
      this.storage.set('pickup', item.name);
    }

    if (this.fromto === 'to') {
      this.storage.set('dropOff', item.name);
    }
    // this.nav.push(SearchCarsPage);
    this.nav.pop();
  }
}
