import {Component} from "@angular/core";
import {LoadingController, NavController, NavParams, ToastController} from "ionic-angular";
import {EventDTO} from "../../services/event-service";
import {HomePage} from "../home/home";
import {Settings} from "../../services/settings";

export interface EventCheckoutDTO {
  event: EventDTO;
  localityCount0: number;
  localityCount1: number;
}

@Component({
  selector: 'page-event-checkout',
  templateUrl: 'event-checkout.html'
})
export class EventCheckoutPage {
  // event data
  public eventCheckout: EventCheckoutDTO;
  public date = new Date();

  public totalPayment: number = 0;
  public ticketsCount: number = 0;

  public paymethods = 'creditcard';

  public avatarImage: string = 'assets/img/avatar.svg';

  constructor(
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public settings: Settings)
  {
    this.eventCheckout = navParams.get('generalParam');
    console.info('Ini check out');
    console.info(this.eventCheckout);

    let eventDate: string = this.eventCheckout.event.eventfein;
    this.date =
      new Date(
        Number(eventDate.substr(0, 4)),
        Number(eventDate.substr(5, 2)) - 1,
        Number(eventDate.substr(8, 2)),
        Number(eventDate.substr(11, 2)),
        Number(eventDate.substr(14, 2)),
        Number(eventDate.substr(17, 2)));
    this.totalPayment =
      this.eventCheckout.localityCount0 * this.eventCheckout.event.localidades[0].locaprec
        + this.eventCheckout.localityCount1 * this.eventCheckout.event.localidades[1].locaprec;
    this.ticketsCount = this.eventCheckout.localityCount0 + this.eventCheckout.localityCount1;
    this.avatarImage = this.settings.allSettings.avatarImage;
  }

  send()
  {
    // send booking info
    let loader = this.loadingCtrl.create({
      content: 'Sending payment of ' + this.totalPayment + '<br>User: ' + this.settings.allSettings.userId + '<br>Please wait...'
    });
    // show message
    let toast = this.toastCtrl.create({
      showCloseButton: true,
      cssClass: 'profile-bg',
      message: 'Payment process was successful!',
      duration: 2000,
      position: 'bottom'
    });

    loader.present().then(() =>
    {
      console.info('Placeholder for sending payment');
    });

    setTimeout(() => {
      loader.dismiss();
      toast.present();
      // back to home page
      this.nav.setRoot(HomePage);
    }, 5000)
  }
}
