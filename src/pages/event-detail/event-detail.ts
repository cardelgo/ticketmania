import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {EventDTO} from "../../services/event-service";
import {EventCheckoutDTO, EventCheckoutPage} from "../event-checkout/event-checkout";
import {Settings} from "../../services/settings";
import {LoginPage} from "../login/login";
import {LocationSelect} from "../location-select/location-select";

@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html'
})
export class EventDetailPage {
  // event info
  public event: EventDTO;
  public localityCount0 = 2;
  public localityCount1 = 0;
  public totalCost = 0;

  constructor(public nav: NavController, public navParams: NavParams, public settings: Settings) {
    this.event = navParams.get('eventKey');
    this.calculateCost();
  }

  // minus adult when click minus button
  minusLocalityCount0() {
    this.localityCount0--;
    this.calculateCost();
  }

  // plus adult when click plus button
  plusLocalityCount0() {
    this.localityCount0++;
    this.calculateCost();
  }

  // minus localityCount1 when click minus button
  minusLocalityCount1() {
    this.localityCount1--;
    this.calculateCost();
  }

  // plus localityCount1 when click plus button
  plusLocality1Count1() {
    this.localityCount1++;
    this.calculateCost();
  }

  calculateCost()
  {
    this.totalCost =
      this.localityCount0 * this.event.localidades[0].locaprec
        + this.localityCount1 * this.event.localidades[1].locaprec;
  }

  // go to checkout page
  checkout() {
    console.info("Pagar " + this.totalCost);
    let connected: boolean = this.settings.allSettings.connected;

    let eventCheckout: EventCheckoutDTO =
      {
        event: this.event,
        localityCount0: this.localityCount0,
        localityCount1: this.localityCount1
      };

    if(!connected)
    {
      this.nav.push(LoginPage, {generalParam: eventCheckout, targetPage: EventCheckoutPage});
    }
    else
    {
      this.nav.push(EventCheckoutPage, {generalParam : eventCheckout});
    }
  }

  openLocation()
  {
    //this.nav.push(LocationSelect, {eventKey: this.event});
    this.nav.push(LocationSelect, {eventKey: this.event});
  }

}
