import {Component} from "@angular/core";
import {NavController, NavParams, ToastController} from "ionic-angular";
import {LoginPage} from "../login/login";
import {Page} from "ionic-angular/navigation/nav-util";
import {LoginService} from "../../services/login-service";
import {Settings} from "../../services/settings";


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage
{
  private targetPage : Page = this.navParams.get('targetPage');
  private generalParam : any = this.navParams.get('generalParam');

  public txtName: string = 'Enrique I';
  public txtEmail: string = 'cardelgo@hotmail.com';
  public txtPassword: string = '123';

  constructor(
    public navParams: NavParams,
    public nav: NavController,
    public toastCtrl: ToastController,
    public loginService: LoginService,
    public settings: Settings,)
  {
  }

  register()
  {
    this.loginService.register(this.txtName, this.txtEmail, this.txtPassword).then(value =>
    {
      if(value['status'] === 1)
      {
        this.settings.allSettings.userId = this.txtEmail;
        this.settings.allSettings.userName = this.txtName;
        this.settings.allSettings.connected = true;
        this.settings.save().then(() =>
          this.nav.push(this.targetPage, {generalParam : this.generalParam}));
      }
      else
      {
        let errorToast = this.toastCtrl.create({
          message: 'Failed trying to register',
          duration: 3000,
          position: 'top',
          cssClass: 'dark-trans',
          closeButtonText: 'OK',
          showCloseButton: true
        });
        errorToast.present();
      }
    });
  }

  login()
  {
    this.nav.push(LoginPage, {targetPage: this.targetPage, generalParam : this.generalParam});
  }
}
