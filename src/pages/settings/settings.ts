import {Component} from "@angular/core";
import {NavController, ToastController} from "ionic-angular";
import {HomePage} from "../home/home";
import {Settings} from "../../services/settings";


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  public apiUrl: string;

  constructor(public toastCtrl: ToastController, public nav: NavController, public settings: Settings)
  {
  }

  // logout
  saveSettings()
  {
    let toast = this.toastCtrl.create({
      showCloseButton: true,
      message: 'Save settings!',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
    if(this.apiUrl != undefined)
    {
      this.settings.allSettings.apiUrl = this.apiUrl;
      this.settings.save();
    }
    this.nav.setRoot(HomePage);
  }
}

