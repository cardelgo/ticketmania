import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {EventDTO, EventService} from "../../services/event-service";
import {EventDetailPage} from "../event-detail/event-detail";

@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})
export class EventsPage {
  // list of events
  public events: EventDTO[];
  public eventsCount: number;

  constructor(public nav: NavController, public eventService: EventService) {
    eventService.getAll().then(response => {
      this.events = response.data;
      this.eventsCount = this.events.length;
    });
  }

  viewEventDetail(event) {
    this.nav.push(EventDetailPage, {eventKey: event});
  }

  likeEvent(event: EventDTO)
  {
    event.eventlike ++;
    this.eventService.updateLikeEvent(event.eventid);
  }

  dislikeEvent(event: EventDTO)
  {
    event.eventdili ++;
    this.eventService.updateDislikeEvent(event.eventid);
  }
}
