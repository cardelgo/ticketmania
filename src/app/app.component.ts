import {Component, ViewChild} from "@angular/core";
import {Nav, Platform, ToastController} from "ionic-angular";

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';

import {LocalWeatherPage} from "../pages/local-weather/local-weather";

import {HomePage} from "../pages/home/home";
import {Settings} from "../services/settings";
import {ImagePicker} from "@ionic-native/image-picker";
import {OneSignal, OSNotificationPayload} from "@ionic-native/onesignal";
import {EventDTO, EventService} from "../services/event-service";
import {EventDetailPage} from "../pages/event-detail/event-detail";

const sender_id = '778100826841'; //Google Proyect Sender ID
const oneSignalAppId = '47ffb9b0-7e9f-45cd-b2fc-300b6a43794d'; //One Signal API ID

export interface MenuItem
{
    title: string;
    component: any;
    icon: string;
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //TODO
  //
  rootPage: any = HomePage;
  appMenuItems: Array<MenuItem>;
  public avatarImage: string = 'assets/img/avatar.svg';

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard,
    public settings: Settings,
    public imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public oneSignal: OneSignal,
    public eventService: EventService)
  {
    this.initializeApp();

    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Local Weather', component: LocalWeatherPage, icon: 'partly-sunny'}
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      if(this.platform.is('mobile') && this.platform.is('cordova'))
      {
        //*** Control Splash Screen
        this.splashScreen.show();
        this.splashScreen.hide();

        //*** Control Status Bar
        this.statusBar.styleDefault();
        this.statusBar.overlaysWebView(false);

        //*** Control Keyboard
        this.keyboard.disableScroll(true);

        this.oneSignal.startInit(oneSignalAppId, sender_id);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        this.oneSignal.handleNotificationReceived().subscribe(data =>
          this.onPushReceived(data.payload));
        this.oneSignal.handleNotificationOpened().subscribe(data =>
          this.onPushOpened(data.notification.payload));
        this.oneSignal.endInit();
      }

      this.settings.load().then(() => {
        this.settings.setValue('connected', false);
        this.avatarImage = this.settings.allSettings.avatarImage;
      });


    });
  }

  private onPushReceived(payload: OSNotificationPayload)
  {
    console.info('Push received:' + payload.additionalData['eventId']);
  }

  private onPushOpened(payload: OSNotificationPayload) {
    let paramEventId = payload.additionalData['eventId'];
    this.eventService.getAll().then(value =>
    {
      let foundEvent : EventDTO = value.data.find(value1 => value1.eventid == paramEventId);
      if( foundEvent != undefined)
      {
        this.nav.push(EventDetailPage, {eventKey: foundEvent});
      }
      else
      {
        confirm('Event with id ' + paramEventId + ' was not found');
      }
    })
  }

  openPage(page)
  {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  changeProfilePicture()
  {
    this.imagePicker.getPictures({maximumImagesCount: 1})
      .then(value =>
      {
        this.avatarImage = value[0];
        this.settings.setValue('avatarImage', this.avatarImage);
        console.info('Got picture ' + value);

        let toast = this.toastCtrl.create({
          showCloseButton: true,
          cssClass: 'profile-bg',
          message: 'Got picture all ' + value,
          duration: 5000,
          position: 'bottom'
        });
        toast.present();
        toast.setMessage('Got picture first: ' + value[0]);
        toast.present();

      })
      .catch(reason => console.error('Error getting pictures ' + reason));

  }

  logout()
  {
    this.settings.allSettings.connected = false;
    this.settings.allSettings.userId = null;
    this.settings.allSettings.avatarImage = null;
    this.settings.allSettings.userName = null;
    this.settings.save();
    this.nav.setRoot(HomePage);
  }
}

