import {NgModule} from "@angular/core";
import {IonicApp, IonicModule} from "ionic-angular";
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { Storage, IonicStorageModule } from '@ionic/storage';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';
import { Geolocation } from '@ionic-native/geolocation';

import {EventService} from "../services/event-service";
import {WeatherProvider} from "../services/weather";

import {MyApp} from "./app.component";

import {SettingsPage} from "../pages/settings/settings";
import {EventCheckoutPage} from "../pages/event-checkout/event-checkout";
import {HomePage} from "../pages/home/home";
import {LoginPage} from "../pages/login/login";
import {NotificationsPage} from "../pages/notifications/notifications";
import {RegisterPage} from "../pages/register/register";
import {SearchLocationPage} from "../pages/search-location/search-location";
import {EventDetailPage} from "../pages/event-detail/event-detail";
import {EventsPage} from "../pages/events/events";
import {LocalWeatherPage} from "../pages/local-weather/local-weather";

import {Api} from "../services/api";
import {Settings, SettingsValues} from "../services/settings";
import {LoginService} from "../services/login-service";

import {FingerprintAIO} from "@ionic-native/fingerprint-aio";
import {ImagePicker} from "@ionic-native/image-picker";
import {OneSignal} from "@ionic-native/onesignal";
import {LocationSelect} from "../pages/location-select/location-select";
import {GoogleMaps} from "../services/googlemaps.service";
import {Connectivity} from "../services/connectivity.service";
import {Network} from "@ionic-native/network";

export function provideSettings (storage: Storage) {
  return new Settings(storage, <SettingsValues>{
    userId: null,
    connected: false,
    apiUrl: 'http://localhost/mytickets',
    avatarImage: 'assets/img/avatar.svg',
    userName: 'Unidentified user'
  });
}

@NgModule({
  declarations: [
    MyApp,
    SettingsPage,
    EventCheckoutPage,
    HomePage,
    LoginPage,
    LocalWeatherPage,
    NotificationsPage,
    RegisterPage,
    SearchLocationPage,
    EventDetailPage,
    LocationSelect,
    EventsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot({
      name: '__ionic3_start_theme',
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SettingsPage,
    EventCheckoutPage,
    HomePage,
    LoginPage,
    LocalWeatherPage,
    NotificationsPage,
    RegisterPage,
    SearchLocationPage,
    EventDetailPage,
    LocationSelect,
    EventsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    EventService,
    WeatherProvider,
    LoginService,
    Api,
    {provide: Settings, useFactory: provideSettings, deps: [Storage]},
    FingerprintAIO,
    ImagePicker,
    OneSignal,
    GoogleMaps,
    Connectivity,
    Network,
    Geolocation,
  ]
})

export class AppModule
{
}
