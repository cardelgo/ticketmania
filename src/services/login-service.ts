import {Injectable} from "@angular/core";
import {Api} from "./api";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoginService
{
  constructor(public api: Api, public http: HttpClient)
  {
  }

  authenticate(email: string, password: string)
  {
    return this.api.get('api.php', {key: 'auth', usuaemai: email, usuapass: password})
  }

  register(name: string, email: string, password: string)
  {
    return this.api.get(
      'api.php',
      {
        key: 'register',
        usuaapel: 'Apellido',
        usuacedu: '112233',
        usuatele: '555123',
        usuanomb: name,
        usuaemai: email,
        usuapass: password})
  }

}
