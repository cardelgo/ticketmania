import {Injectable} from "@angular/core";
import {Api} from "./api";
import { HttpClient } from '@angular/common/http';


export interface LocalidadeDTO {
  locaid: number;
  eventid_fk: number;
  locadesc: string;
  locaprec: number;
  locacabo: number;
}

export interface ParticipanteDTO {
  partid: number;
  partnomb: string;
}

export interface EventDTO {
  eventid: number;
  eventtype: number;
  eventnomb: string;
  eventdesc: string;
  eventpromo: number;
  eventfepu: string;
  eventfein: string;
  eventfefi: string;
  eventlike: number;
  eventdili: number;
  eventloca: string;
  eventlati: number;
  eventlong: number;
  images: string[];
  localidades: LocalidadeDTO[];
  participantes: ParticipanteDTO[];
}

export interface EventCatalogDTO {
  status: string,
  data: EventDTO[]
}

@Injectable()
export class EventService
{
  constructor(public api: Api, public http: HttpClient)
  {
  }

  getAll() : Promise<EventCatalogDTO>
  {
    return this.api.get<EventCatalogDTO>('api.php', {key: 'getAllEvents'})
  }

  updateLikeEvent(eventId: number)
  {
    this.api.get('api.php', {key: 'likes', eventid: eventId})
  }

  updateDislikeEvent(eventId: number)
  {
    this.api.get('api.php', {key: 'dislikes', eventid: eventId})
  }
}
