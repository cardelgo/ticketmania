import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Headers, RequestOptions} from "@angular/http";
import {Settings} from "./settings";

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {

  constructor(public http: HttpClient, public settings: Settings)
  {
  }

  get<T>(endpoint: string, params?: any, reqOpts?: any) {
    let myHeaders : Headers = new Headers();
    //myHeaders.set('Accept', 'application/json; charset=utf-8');

    if (!reqOpts) {
      reqOpts = new RequestOptions({
        headers : myHeaders
      });
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    return new Promise(resolve => {
      let targetApi : string = this.settings.allSettings.apiUrl;
      this.http.get<T>(targetApi + '/' + endpoint, reqOpts).subscribe(data => {
        resolve(data);
      }, err => {
        console.log('API error->');
        console.log(err);
      });
    });
  }

  /*
  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http.post(this.url + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
  }
  */
}
