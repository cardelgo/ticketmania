/*

ionic cordova plugin add cordova-plugin-geolocation --save
npm install  @ionic-native/geolocation --save
npm install @types/google-maps --save

import { GoogleMaps } from '../providers/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
*/
import { Injectable } from '@angular/core';
import { Connectivity } from './connectivity.service';
import { Geolocation } from '@ionic-native/geolocation';
import {} from '@types/googlemaps';
import LatLng = google.maps.LatLng;
import Place = google.maps.Place;

@Injectable()
export class GoogleMaps {

  mapElement: any;
  pleaseConnect: any;
  map: google.maps.Map;
  mapInitialised: boolean = false;
  //mapLoaded: any;
  //mapLoadedObserver: any;
  //currentMarker: any;
  apiKey: string = "AIzaSyAEtfLR_Qbe4JUXaKlkPIY3uOprgp-_Y3U";

  constructor(public connectivityService: Connectivity, public geoLocation: Geolocation)
  {
  }

  init(mapElement: any, pleaseConnect: any, defaultPlace? : Place): Promise<any> {

    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;

    return this.loadGoogleMaps(defaultPlace);
  }

  loadGoogleMaps(defaultPlace? : Place): Promise<any> {

    return new Promise((resolve) => {

      if(typeof google == "undefined" || typeof google.maps == "undefined"){

        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();

        if(this.connectivityService.isOnline()){

          window['mapInit'] = () => {

            this.initMap(defaultPlace).then(() => {
              resolve(true);
            });

            this.enableMap();
          }

          let script = document.createElement("script");
          script.id = "googleMaps";


          if(this.apiKey)
          {
            //script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit&libraries=places';
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit&libraries=places';
          }
          else
          {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
          }

          document.body.appendChild(script);

        }
      }
      else
      {

        if(this.connectivityService.isOnline()){
          this.initMap(defaultPlace);
          this.enableMap();
        }
        else {
          this.disableMap();
        }

        resolve(true);

      }

      this.addConnectivityListeners();

    });

  }

  initMap(defaultPlace? : Place): Promise<any> {

    return new Promise((resolve) => {

      this.geoLocation.getCurrentPosition().then((position) => {

        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        if(defaultPlace)
        {
          latLng = <LatLng>defaultPlace.location;
        }
        console.info(defaultPlace);
        console.info(JSON.stringify(defaultPlace));
        console.info(latLng)

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: true,

        }

        this.map = new google.maps.Map(this.mapElement, mapOptions);
        this.mapInitialised = true;
        if(defaultPlace)
        {
          this.addMarker(defaultPlace.placeId);
        }

        resolve(true);

      });

    });

  }
 /*          backgroundColor: 'white',
          controls: {
            'compass': true,
            'myLocationButton': true,
            'indoorPicker': true,
            'zoom': true
          },
          camera: {
            'latLng': location,
            'tilt': 30,
            'zoom': 15,
            'bearing': 50
          }
*/
  disableMap(): void {

    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "block";
    }

  }

  enableMap(): void {

    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "none";
    }

  }

  addConnectivityListeners(): void {

    this.connectivityService.watchOnline().subscribe(() => {

      setTimeout(() => {

        if(typeof google == "undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps();
        }
        else {
          if(!this.mapInitialised){
            this.initMap();
          }

          this.enableMap();
        }

      }, 2000);

    });

    this.connectivityService.watchOffline().subscribe(() => {
      this.disableMap();
    });
  }

  addMarker(markerTitle: string) {

    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter()
    });



    //let pos : google.maps.LatLng = marker.getPosition() ;

    let content = "<h4>" + markerTitle + "</h4>";

    marker.setDraggable(true);
    this.addInfoWindow(marker, content);


    google.maps.event.addListener(marker, 'dragstart', () => {
      //infoWindow.open(this.googleMaps.map, marker);
      let pos: google.maps.LatLng = marker.getPosition();

      console.log("Start Posititon : Lat ->" + pos.toJSON().lat + " Long ->" + pos.toJSON().lng)
    });

    google.maps.event.addListener(marker, 'drag', () => {
      //infoWindow.open(this.googleMaps.map, marker);
      let pos: google.maps.LatLng = marker.getPosition();

      console.log("Drag Posititon : Lat ->" + pos.toJSON().lat + " Long ->" + pos.toJSON().lng)
    });

    google.maps.event.addListener(marker, 'dragend', () => {
      //infoWindow.open(this.googleMaps.map, marker);
      let pos: google.maps.LatLng = marker.getPosition();

      console.log("End Posititon : Lat ->" + pos.toJSON().lat + " Long ->" + pos.toJSON().lng)
    });
  }

  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

}
