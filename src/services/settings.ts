
import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';

export interface SettingsValues
{
  userId: string
  userName: string
  apiUrl: string
  connected: boolean
  avatarImage: string
}

/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
@Injectable()
export class Settings {
  private SETTINGS_KEY: string = '_config_settings_4';

  settings: SettingsValues;

  _defaults: SettingsValues;

  constructor(public storage: Storage, defaults: SettingsValues) {
    this._defaults = defaults;
    this.load();
  }

  load() {
    return this.storage.get(this.SETTINGS_KEY).then((value) => {
      if (value) {
        this.settings = value;
        return this._mergeDefaults(this._defaults);
      } else {
        return this.setAll(this._defaults).then((val) => {
          this.settings = val;
        })
      }
    });
  }

  _mergeDefaults(defaults: SettingsValues) {
    for (let k in defaults) {
      if (!(k in this.settings)) {
        this.settings[k] = defaults[k];
      }
    }
    return this.setAll(this.settings);
  }

  merge(settings: SettingsValues) {
    for (let k in settings) {
      this.settings[k] = settings[k];
    }
    return this.save();
  }

  setValue(key: string, value: any) {
    this.settings[key] = value;
    return this.storage.set(this.SETTINGS_KEY, this.settings);
  }

  setAll(value: SettingsValues) {
    return this.storage.set(this.SETTINGS_KEY, value);
  }

  /*
  getValue(key: string) {
    return this.storage.get(this.SETTINGS_KEY)
      .then(settings => {
        return settings[key];
      });
  }
  */

  save() {
    return this.setAll(this.settings);
  }

  get allSettings() {
    return this.settings;
  }
}

